#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import pymysql

# path_dir = "/var/lib/tomcat7/webapps/vod/ipcam3"
path_dir = "/home/hecas/Videos/"
file_path = "/home/etri/Downloads/vod/ipcam3"

db_name = 'hecas'
table_name = 'VOD'

mon_dict = {"Jan": "01", "Feb": "02", "Mar": "03", "Apr": "04", "May": "05", "Jun": "06", "Jul": "07", "Aug": "08",
            "Sep": "09", "Oct": "10", "Nov": "11", "Dec": "12"}

min_list = ['00', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10',
            '11', '12', '13', '14', '15', '16', '17', '18', '19', '20',
            '21', '22', '23', '24', '25', '26', '27', '28', '29', '30',
            '31', '32', '33', '34', '35', '36', '37', '38', '39', '40',
            '41', '42', '43', '44', '45', '46', '47', '48', '49', '50',
            '51', '52', '53', '54', '55', '56', '57', '58', '59', '60']


def parse_filename():
    # 현재 위치(.)의 파일을 모두 가져온다.

    for filename in os.listdir(path_dir):
        if (filename.split(".")[-1].lower() == 'mp4'):
            # print(type(filename.split("_")))
            # print(filename.split("_"))

            if len(filename.split("_")) == 8:
                global VOD_DAY
                global VOD_HOUR
                global VOD_YEAR
                VOD_DAY = filename.split("_")[3]
                print(VOD_DAY)
                VOD_HOUR = filename.split("_")[4]
                print(VOD_HOUR)
                VOD_YEAR = filename.split("_")[7].split(".")[0]
                print(VOD_YEAR)

                for mon in mon_dict.keys():
                    if mon == filename.split("_")[1]:
                        global VOD_MON
                        VOD_MON = mon_dict[filename.split("_")[1]]
                        print(VOD_MON)

                for min in min_list[0:16]:
                    global VOD_Name
                    global VOD_URL
                    # print(min)
                    if min == filename.split("_")[5]:
                        VOD_Name = '00 ~ 15분'
                        VOD_URL = 'http://localhost:18080/vod/ipcam3/' + filename
                        print(VOD_URL)
                        print(filename)
                        print(VOD_Name)
                        break
                    else:
                        continue
                for min in min_list[16:31]:
                    if min == filename.split("_")[5]:
                        VOD_Name = '15 ~ 30분'
                        VOD_URL = 'http://localhost:18080/vod/ipcam3/' + filename
                        print(filename)
                        print(VOD_Name)
                        break
                    else:
                        continue
                for min in min_list[31:46]:
                    if min == filename.split("_")[5]:
                        VOD_Name = '30 ~ 45분'
                        VOD_URL = 'http://localhost:18080/vod/ipcam3/' + filename
                        print(filename)
                        print(VOD_Name)
                        break
                    else:
                        continue
                for min in min_list[45:61]:
                    if min == filename.split("_")[5]:
                        VOD_Name = '45 ~ 60분'
                        VOD_URL = 'http://localhost:18080/vod/ipcam3/' + filename
                        print(filename)
                        print(VOD_Name)
                        break
                    else:
                        continue
                insert_mysql(VOD_YEAR, VOD_MON, VOD_DAY, VOD_HOUR, VOD_URL)

            else:
                # VOD_MON = filename.split("_")[1]
                # print(VOD_MON)
                global VOD_DAY
                VOD_DAY = filename.split("_")[2]
                print(VOD_DAY)
                global VOD_HOUR
                VOD_HOUR = filename.split("_")[3]
                print(VOD_HOUR)
                global VOD_YEAR
                VOD_YEAR = filename.split("_")[6].split(".")[0]
                print(VOD_YEAR)
                for mon in mon_dict.keys():
                    if mon == filename.split("_")[1]:
                        global VOD_MON
                        VOD_MON = mon_dict[filename.split("_")[1]]
                        print(VOD_MON)

                for min in min_list[0:16]:
                    global VOD_Name
                    # print(min)
                    if min == filename.split("_")[4]:
                        VOD_Name = '00 ~ 15분'
                        VOD_URL = 'http://localhost:18080/vod/ipcam3/' + filename
                        print(filename)
                        print(VOD_Name)
                        break
                    else:
                        continue
                for min in min_list[16:31]:
                    if min == filename.split("_")[4]:
                        VOD_Name = '15 ~ 30분'
                        VOD_URL = 'http://localhost:18080/vod/ipcam3/' + filename
                        print(filename)
                        print(VOD_Name)
                        break
                    else:
                        continue
                for min in min_list[31:46]:
                    if min == filename.split("_")[4]:
                        VOD_Name = '30 ~ 45분'
                        VOD_URL = 'http://localhost:18080/vod/ipcam3/' + filename
                        print(filename)
                        print(VOD_Name)
                        break
                    else:
                        continue
                for min in min_list[45:61]:
                    if min == filename.split("_")[4]:
                        VOD_Name = '45 ~ 60분'
                        VOD_URL = 'http://localhost:18080/vod/ipcam3/' + filename
                        print(filename)
                        print(VOD_Name)
                        break
                    else:
                        continue

                insert_mysql(VOD_YEAR, VOD_MON, VOD_DAY, VOD_HOUR, VOD_URL, VOD_Name)


def create_db():
    conn = pymysql.connect(host='localhost',
                           user='hecas',
                           password='hecas123',
                           charset='utf8mb4')

    try:
        with conn.cursor() as cursor:
            sql = 'CREATE DATABASE ' + db_name
            cursor.execute(sql)
        conn.commit()
        print("Create database!")
    finally:
        conn.close()


def create_table():
    conn = pymysql.connect(host='localhost',
                           user='hecas',
                           password='hecas123',
                           db=db_name,
                           charset='utf8mb4')

    try:
        with conn.cursor() as cursor:
            sql = '''
                CREATE TABLE VOD (
                    WORK_SEQ int(11) NOT NULL,
                    BOX_ID int(11) NOT NULL,
                    VOD_TYPE varchar(2) NOT NULL,
                    VOD_YEAR varchar(4) NOT NULL,
                    VOD_MON varchar(2) NOT NULL,
                    VOD_DAY varchar(2) NOT NULL,
                    VOD_HOUR varchar(2) NOT NULL,
                    VOD_URL varchar(300) NOT NULL,
                    VOD_Name varchar(100) NOT NULL,
                    CRE_USER varchar(20) NOT NULL,
                    CRE_UPDATE datetime NOT NULL,
                    UPD_USER varchar(20) NOT NULL,
                    UPD_UPDATE datetime NOT NULL
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8
    '''
            cursor.execute(sql)
        conn.commit()
        print("Create Table!")
    finally:
        conn.close()


def drop_table():
    conn = pymysql.connect(host='localhost',
                           user='hecas',
                           password='hecas123',
                           db=db_name,
                           charset='utf8mb4')

    try:
        with conn.cursor() as cursor:
            sql = 'DROP TABLE VOD'
            cursor.execute(sql)
        conn.commit()
        print("Drop Table!")
    finally:
        conn.close()


def insert_mysql(VOD_YEAR, VOD_MON, VOD_DAY, VOD_HOUR, VOD_URL, VOD_Name):
    conn = pymysql.connect(host='localhost', user='hecas', password='hecas123',
                           db='hecas', charset='utf8')

    try:
        # INSERT
        with conn.cursor() as curs:
            sql = "insert into VOD (WORK_SEQ, BOX_ID, VOD_TYPE, VOD_YEAR, VOD_MON, VOD_DAY, VOD_HOUR, VOD_URL, VOD_Name, CRE_USER, CRE_UPDATE, UPD_USER, UPD_UPDATE) " \
                  "values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s )"
            # print(parse_filename().)
            curs.execute(sql, ('2', '1', '01', VOD_YEAR, VOD_MON, VOD_DAY, VOD_HOUR, VOD_URL, VOD_Name, '','2018-10-16','', '2018-10-16'))

            conn.commit()
            print("Insert Data Done!")

        # SELECT
        with conn.cursor() as curs:
            sql = "select * FROM VOD"
            curs.execute(sql)
            rs = curs.fetchall()
            for row in rs:
                print(row)

    finally:
        conn.close()


if __name__ == "__main__":
    parse_filename()
    # create_db()
    # create_table()
    # drop_table()
    # insert_mysql()
